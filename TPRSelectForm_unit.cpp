//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "tpr.h"
#include "TPRSelectForm_unit.h"
#include "TPRForm_unit.h"
#include "TPRDLLInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"

   int SortColumn;
   bool Up;
//---------------------------------------------------------------------------
__fastcall TTPRSelectForm::TTPRSelectForm(TComponent* Owner)
	: TForm(Owner)
{
	randomize();

   SortColumn=1;
   Up=false;
	TPRs = new TList();
	TPRs1 = new TList();
	TTPRDLLInterface::LoadTPRs(TPRs);

   TPRs1->Assign(TPRs);

   Sort();
	FillGrid();
	MMForm = new TTPRForm(this);

}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::SelectTPR(TTPR* TPR)
{
	Caption="����� ���";
	CancelButton->Visible=true;
	OkButton->Caption="�������";
	memset(TPR,0,sizeof(TTPR));
   IsManage=false;
   HelpContext=3001;
	bool result=(ShowModal()==mrOk);
	if(result && SelectedTPR)
		{
		memcpy(TPR,SelectedTPR,sizeof(TTPR));
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::CreateParams(Controls::TCreateParams &Params)
{
	TForm::CreateParams(Params);
	TForm* f=(TForm*)Owner;
	if(f!=NULL)
		Params.WndParent=f->Handle;
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::FillGrid()
{
	MMGrid->Cells[1][0]="������";
	MMGrid->Cells[2][0]="��";
	MMGrid->Cells[3][0]="���. �";
	MMGrid->Cells[4][0]="���";
	MMGrid->Cells[5][0]="����� ���.";
	int c=TPRs->Count;
	if(c==0)
		{
		MMGrid->RowCount=2;
		MMGrid->Cells[0][1]="";
		MMGrid->Cells[1][1]="";
		MMGrid->Cells[2][1]="";
		MMGrid->Cells[3][1]="";
		MMGrid->Cells[4][1]="";
		MMGrid->Cells[5][1]="";
		return;
		}
	MMGrid->RowCount=c+1;
	for(int i=0;i<c;i++)
		{
		TTPR* p=(TTPR*)(TPRs->Items[i]);
		if(!p) continue;
		MMGrid->Cells[0][i+1]=i+1;
		MMGrid->Cells[1][i+1]=p->Model;
		MMGrid->Cells[2][i+1]=p->D;
		MMGrid->Cells[3][i+1]=p->SN;
		MMGrid->Cells[4][i+1]=p->UUN;
		MMGrid->Cells[5][i+1]=p->Location;
		}
}
//---------------------------------------------------------------------------
__fastcall TTPRSelectForm::~TTPRSelectForm()
{
	delete MMForm;
	TTPRDLLInterface::SaveTPRs(TPRs1);
	int c=TPRs1->Count;
	for(int i=0;i<c;i++)
		{
		TTPR* p=(TTPR*)(TPRs1->Items[i]);
		delete p;
		}
	delete TPRs;
   delete TPRs1;
}
//---------------------------------------------------------------------------
TTPR* __fastcall TTPRSelectForm::GetTPRIndex()
{
	if(TPRs->Count<=0) return 0;
	TTPR* p = (TTPR*)(TPRs->Items[MMGrid->Row-1]);
	return p;
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::ViewButtonClick(TObject *Sender)
{
	TTPR* fd=SelectedTPR;
	if(fd)
		MMForm->ViewTPR(fd);
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::AddButtonClick(TObject *Sender)
{
	TTPR* p=new TTPR;
	memset(p,0,sizeof(TTPR));
	p->ID = random(MaxInt);
   p->version=1;
	if(MMForm->NewTPR(p))
		{
		TPRs1->Add(p);

      Edit1->Text="";
      Edit1Change(0);
     	MMGrid->Row=TPRs->IndexOf(p)+1;
//  		FillGrid();
		}
	else
		delete p;
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::EditButtonClick(TObject *Sender)
{
	TTPR* fd=SelectedTPR;
	if(fd)
		{
		bool r = MMForm->EditTPR(fd);
		if(r)
			FillGrid();
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::DeleteButtonClick(TObject *Sender)
{
	AnsiString s("������� �� ������ ���:\n\n\t������\t\t");
	TTPR* p=SelectedTPR;
	if(p==NULL) return;
	s+=p->Model;
	s+="\n\t��\t\t";
	s+=p->D;
	s+="\n\t���. �\t\t";
	s+=p->SN;
	s+="\n\t���\t\t";
	s+=p->UUN;
	s+="\n\t����� ���������\t";
	s+=p->Location;
	if(Application->MessageBox(s.c_str(),
		"�������� ���",MB_YESNO|MB_ICONQUESTION)==IDYES)
		{
		TTPRDLLInterface::DeleteTPR(p->ID);
		TPRs1->Remove(p);
		delete p;
		Edit1Change(0);
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::Manage()
{
   Caption="���������� ������� ���";
	CancelButton->Visible=false;
	OkButton->Caption="�������";
   IsManage=true;
   HelpContext=3006;
   Edit1->Text="";
	ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::MMGridDblClick(TObject *Sender)
{
   int Column, Row;
   TPoint pt=Mouse->CursorPos;
   pt=MMGrid->ScreenToClient(pt);
   MMGrid->MouseToCell(pt.x, pt.y, Column, Row);

	if( (TPRs->Count<=0) || (Row<1) ) return;
	TTPR* p = (TTPR*)(TPRs->Items[MMGrid->Row-1]);
	if(!p) return;
   if(IsManage)
      EditButtonClick(this);
   else
      ModalResult=mrOk;
}
//---------------------------------------------------------------------------

void __fastcall TTPRSelectForm::MMGridDrawCell(TObject *Sender, int ACol,
      int ARow, TRect &Rect, TGridDrawState State)
{
   if((ARow>0) ||(SortColumn==0) || (SortColumn>5) || (ACol!=SortColumn)) return;
   int i=0;
   if(Up)i=1;
   ImageList1->Draw(MMGrid->Canvas,Rect.right - 13,Rect.top+5,i);
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::MMGridMouseDown(TObject *Sender,
      TMouseButton Button, TShiftState Shift, int X, int Y)
{
   int Column, Row;
   MMGrid->MouseToCell(X, Y, Column, Row);
   if( (Row==0) && (Column>0))
      {
      if(SortColumn==Column)
         Up=!Up;
      else
         SortColumn=Column;
      Sort();
      FillGrid();
      }
   MMGrid->Repaint();
}
//---------------------------------------------------------------------------
int __fastcall TPRCompareNames(void *Item1, void *Item2)
{
   TTPR* p1=(TTPR*)(Item1);
   TTPR* p2=(TTPR*)(Item2);
   int i=0;
   switch(SortColumn)
      {
      case 1: i=CompareText(p1->Model,p2->Model);
         break;
      case 2: i = CompareText(p1->D,p2->D);
         break;
      case 3: i=CompareText(p1->SN,p2->SN);
         break;
      case 4: i = CompareText(p1->UUN,p2->UUN);
         break;
      case 5: i= CompareText(p1->Location,p2->Location);
         break;
      }
   if(Up) i*= -1;
   return i;
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::Sort()
{
   TPRs->Sort(TPRCompareNames);
}
//---------------------------------------------------------------------------
void __fastcall TTPRSelectForm::Edit1Change(TObject *Sender)
{
   //
   if(Edit1->Text.IsEmpty())
      {
      TPRs->Assign(TPRs1);
      Sort();
      FillGrid();
      }
   else
      {
      TPRs->Clear();
      int c=TPRs1->Count;
      for(int i=0;i<c;i++)
         {
         TTPR* p1=(TTPR*)TPRs1->Items[i];
         if(((AnsiString)(p1->Model)).UpperCase().Pos(Edit1->Text))
            {
            TPRs->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->SN)).UpperCase().Pos(Edit1->Text))
            {
            TPRs->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->UUN)).UpperCase().Pos(Edit1->Text))
            {
            TPRs->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->Location)).UpperCase().Pos(Edit1->Text))
            {
            TPRs->Add(p1);
            continue;
            }
         if(((AnsiString)(p1->D)).UpperCase().Pos(Edit1->Text))
            {
            TPRs->Add(p1);
            continue;
            }
         }
      Sort();
      FillGrid();
      }
}
//---------------------------------------------------------------------------

