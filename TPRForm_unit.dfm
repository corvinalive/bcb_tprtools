object TPRForm: TTPRForm
  Left = 89
  Top = 227
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsDialog
  Caption = 'TPRForm'
  ClientHeight = 246
  ClientWidth = 695
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  Icon.Data = {
    0000010001001010100000000000280100001600000028000000100000002000
    00000100040000000000C0000000000000000000000000000000000000000000
    000000008000008000000080800080000000800080008080000080808000C0C0
    C0000000FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00FFFF
    F000000FFFFFFFFF00000000FFFFFF000AA00EE000FFFF00AAA00EEE00FFF00A
    A000000EE00F00AA000FF000E00000AA00FFFF00EE0000000FFFFFF000000000
    0FFFFFF0000000EE00FFFF00AA0000EE000FF000A000F00EE000000AA00FFF00
    EEE00AAA00FFFF000EE00AA000FFFFFF00000000FFFFFFFFF000000FFFFF0000
    0000000000000000000000000000000000000000000000000000000000000000
    000000000000000000000000000000000000000000000000000000000000}
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object OkButton: TBitBtn
    Left = 6
    Top = 216
    Width = 202
    Height = 25
    TabOrder = 2
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 210
    Top = 216
    Width = 202
    Height = 25
    TabOrder = 3
    Kind = bkCancel
  end
  object GroupBox2: TGroupBox
    Left = 422
    Top = 2
    Width = 269
    Height = 242
    Caption = #1043#1088#1072#1076#1091#1080#1088#1086#1074#1086#1095#1085#1099#1077' '#1093#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080
    TabOrder = 1
    object MXListBox: TListBox
      Left = 4
      Top = 20
      Width = 259
      Height = 163
      Style = lbVirtual
      ItemHeight = 32
      TabOrder = 0
      OnDrawItem = MXListBoxDrawItem
    end
    object EditMXButton: TBitBtn
      Left = 6
      Top = 186
      Width = 127
      Height = 25
      Caption = #1056#1077#1076#1072#1082#1090#1080#1088#1086#1074#1072#1090#1100'...'
      TabOrder = 1
      OnClick = EditMXButtonClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF0000000000000000000000000000000000000000000000000000
        00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
        CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000FFCDB4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
        CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000FFCDB4FFFFFF0000000000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4000000FF
        FFFFFFFFFF000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF00000000000000FFFF000000FFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FFFF00
        0000000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFF00000000FFFF00FFFF0000000000000000000000000000000000000000
        00000000000000000000FFFFFFFFFFFF00000000FFFF00FFFF000000000000FF
        8548FF8548FF8548FF8548FF8548FF8548FF8548FF8548000000FFFFFF000000
        FFFFFF00FFFF0000000000000000000000000000000000000000000000000000
        00000000000000000000000000FFFFFF00FFFF000000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
      Margin = 4
    end
    object DeleteMXButton: TBitBtn
      Left = 136
      Top = 212
      Width = 129
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100'...'
      TabOrder = 4
      OnClick = DeleteMXButtonClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888888888888888888888888888888888888888888888888888888888888
        8888888888888888888880000000000000887999999999999908799999999999
        9908799999999999990887777777777777888888888888888888888888888888
        8888888888888888888888888888888888888888888888888888}
      Margin = 4
    end
    object AddMXButton: TBitBtn
      Left = 6
      Top = 212
      Width = 127
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100'...'
      TabOrder = 3
      OnClick = AddMXButtonClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888888880000888888888887AAA0888888888887AAA0888888888887AAA088
        8888888887AAA0888888877777AAA00000887AAAAAAAAAAAAA087AAAAAAAAAAA
        AA087AAAAAAAAAAAAA08877777AAA0000088888887AAA0888888888887AAA088
        8888888887AAA0888888888887AAA08888888888887778888888}
      Margin = 4
    end
    object ViewButton: TBitBtn
      Left = 136
      Top = 186
      Width = 129
      Height = 25
      Caption = #1055#1088#1086#1089#1084#1086#1090#1088#1077#1090#1100'...'
      TabOrder = 2
      OnClick = ViewButtonClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00333333333333
        333300000000000033000FFFFFFFFFF030000FFFFFFF000000030FFFFFF07887
        00330FFFFF0788E7F0330FFFFF08888780330FFFFF08E88780330FFFFF07EE87
        F0330FFFFFF0788703330FFFFFFF000033330FFFFFFFFFF033330FFFFFFF0000
        33330FFFFFFF080333330FFFFFFF003333330000000003333333}
    end
  end
  object GroupBox1: TGroupBox
    Left = 2
    Top = 2
    Width = 417
    Height = 213
    Caption = #1044#1072#1085#1085#1099#1077' '#1058#1055#1056
    TabOrder = 0
    object Label13: TLabel
      Left = 8
      Top = 160
      Width = 113
      Height = 16
      Caption = #1052#1077#1089#1090#1086' '#1091#1089#1090#1072#1085#1086#1074#1082#1080
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label15: TLabel
      Left = 8
      Top = 184
      Width = 65
      Height = 16
      Caption = #1042#1083#1072#1076#1077#1083#1077#1094
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label1: TLabel
      Left = 8
      Top = 16
      Width = 104
      Height = 16
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel
      Left = 8
      Top = 112
      Width = 115
      Height = 16
      Caption = #1047#1072#1074#1086#1076#1089#1082#1086#1081' '#1085#1086#1084#1077#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel
      Left = 8
      Top = 40
      Width = 50
      Height = 16
      Caption = #1052#1086#1076#1077#1083#1100
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel
      Left = 8
      Top = 64
      Width = 124
      Height = 16
      Caption = #1059#1089#1083#1086#1074#1085#1099#1081' '#1076#1080#1072#1084#1077#1090#1088
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label6: TLabel
      Left = 8
      Top = 88
      Width = 180
      Height = 16
      Caption = #1052#1072#1082#1089#1080#1084#1072#1083#1100#1085#1099#1081' '#1088#1072#1089#1093#1086#1076', '#1084'3/'#1095
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object Label7: TLabel
      Left = 8
      Top = 136
      Width = 28
      Height = 16
      Caption = #1059#1059#1053
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'MS Sans Serif'
      Font.Style = []
      ParentFont = False
    end
    object LocationEdit: TEdit
      Left = 210
      Top = 156
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 6
      Text = 'LocationEdit'
    end
    object OwnerEdit: TEdit
      Left = 210
      Top = 180
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 7
      Text = 'OwnerEdit'
    end
    object CompanyEdit: TEdit
      Left = 210
      Top = 12
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 0
      Text = 'CompanyEdit'
    end
    object SNEdit: TEdit
      Left = 210
      Top = 108
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 4
      Text = 'SNEdit'
    end
    object ModelEdit: TEdit
      Left = 210
      Top = 36
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 1
      Text = 'CompanyEdit'
    end
    object DEdit: TNumberEdit
      Left = 210
      Top = 60
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      Text = '0'
      NumberType = Double
    end
    object QmaxEdit: TNumberEdit
      Left = 210
      Top = 84
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      ParentFont = False
      TabOrder = 3
      Text = '0'
      NumberType = Double
    end
    object UUNEdit: TEdit
      Left = 210
      Top = 132
      Width = 185
      Height = 24
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clBlue
      Font.Height = -13
      Font.Name = 'Courier New'
      Font.Style = []
      MaxLength = 119
      ParentFont = False
      TabOrder = 5
    end
  end
end
