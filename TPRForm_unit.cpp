//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "tpr.h"
#include "TPRForm_unit.h"
#include "TPRMXForm_unit.h"
#include "TPRDLLInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
//---------------------------------------------------------------------------
__fastcall TTPRForm::TTPRForm(TComponent* Owner)
	: TForm(Owner)
{
	MX = new TList;
	DeletedMX = new TList;
   TPRMXForm=new TTPRMXForm(this);
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::CreateParams(Controls::TCreateParams &Params)
{
	TForm::CreateParams(Params);
	TForm* f=(TForm*)Owner;
	Params.WndParent=f->Handle;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::EnableControls()
{
	CompanyEdit->ReadOnly=!CanEdit;
	ModelEdit->ReadOnly=!CanEdit;
	DEdit->ReadOnly=!CanEdit;
	QmaxEdit->ReadOnly=!CanEdit;
	SNEdit->ReadOnly=!CanEdit;
	UUNEdit->ReadOnly=!CanEdit;
	LocationEdit->ReadOnly=!CanEdit;
	OwnerEdit->ReadOnly=!CanEdit;
   EditMXButton->Enabled=CanEdit;
   AddMXButton->Enabled=CanEdit;
   DeleteMXButton->Enabled=CanEdit;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::TPRToForm(TTPR* p)
{
	ID=p->ID;
	CompanyEdit->Text=p->Company;
	ModelEdit->Text=p->Model;
	DEdit->Text=p->D;
	QmaxEdit->Text=p->Qmax;
	SNEdit->Text=p->SN;
	UUNEdit->Text=p->UUN;
	LocationEdit->Text=p->Location;
	OwnerEdit->Text=p->Owner;

	FillMXData();
   FillMXListBox();
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::ViewTPR(TTPR* p)
{
	Caption="�������� ������ ���";
	OkButton->Caption="�������";
	CancelButton->Visible=false;
   HelpContext=3003;
	CanEdit=false;
	IsNew=false;
   ActiveControl=OkButton;
	EnableControls();
	TPRToForm(p);
	ShowModal();
   ClearMXData();
}
//---------------------------------------------------------------------------
bool __fastcall TTPRForm::EditTPR(TTPR* p)
{
	Caption="�������������� ������ ���";
	OkButton->Caption="��������� ��������� � �����";
	CancelButton->Visible=true;
	CancelButton->Caption="����� ��� ���������� ���������";
   HelpContext=3004;
	CanEdit=true;
	IsNew=false;
	EnableControls();
	TPRToForm(p);
   ActiveControl=CompanyEdit;
	if(ShowModal()==mrOk)
		{
		FormToTPR(p);
		TTPRDLLInterface::SaveTPR(p);
		SaveMXData();
		ClearMXData();
      return true;
		}
   ClearMXData();
	return false;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::FormToTPR(TTPR* p)
{
	ID=p->ID;
	strcpy(p->Company,CompanyEdit->Text.c_str());
	strcpy(p->Model,ModelEdit->Text.c_str());
	p->D=DEdit->Text.ToDouble();
	p->Qmax=QmaxEdit->Text.ToDouble();
	strcpy(p->UUN,UUNEdit->Text.c_str());
	strcpy(p->Location,LocationEdit->Text.c_str());
	strcpy(p->Owner,OwnerEdit->Text.c_str());
	strcpy(p->SN,SNEdit->Text.c_str());
}
//---------------------------------------------------------------------------
bool __fastcall TTPRForm::NewTPR(TTPR* p)
{
	Caption="���������� ���";
	OkButton->Caption="��������� ��������� � �����";
	CancelButton->Visible=true;
	CancelButton->Caption="����� ��� ���������� ���������";
   HelpContext=3002;
	CanEdit=true;
	IsNew=true;
	//
	EnableControls();
	TPRToForm(p);
   ActiveControl=CompanyEdit;
	if(ShowModal()==mrOk)
		{
		FormToTPR(p);
      SaveMXData();
		ClearMXData();
		return true;
		}
	else
		return false;
}
//---------------------------------------------------------------------------
__fastcall TTPRForm::~TTPRForm()
{
   delete TPRMXForm;
   delete MX;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::EditMXButtonClick(TObject *Sender)
{
	TTPRMX* mx;
	int c=MXListBox->ItemIndex;
	if(c==-1) return;
	mx=(TTPRMX*) (MX->Items[c]);
	if(mx==NULL) return;
	if(CanEdit)
		{
		if(TPRMXForm->EditMX(mx))
			FillMXListBox();
      }
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::FillMXListBox()
{
	int c=MX->Count;
	MXListBox->Clear();
   MXListBox->Count=c;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::AddMXButtonClick(TObject *Sender)
{
   TTPRMX* mx=new TTPRMX();
   memset(mx,0,sizeof(TTPRMX));
   mx->Date=TDate::CurrentDate();
   if(TPRMXForm->EditMX(mx))
      {
      mx->ID=rand();
      MX->Add(mx);
      FillMXListBox();
      }
   else
      delete mx;
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::DeleteMXButtonClick(TObject *Sender)
{
	TTPRMX* mx;
	int c=MXListBox->ItemIndex;
	if(c==-1) return;
	if(c>=MX->Count) return;
	mx=(TTPRMX*)(MX->Items[c]);
	if(!mx) return;
	AnsiString s("������� ���������� �����������:\n");
	s+="\t";
	s+=mx->Description;
	s+="\n\t���� ����������� ";
	s+=mx->Date.DateString();
	if(Application->MessageBox(s.c_str(),"�������� ����������� �����������",MB_YESNO|MB_ICONSTOP)==ID_YES)
		{
		MX->Extract(mx);
      DeletedMX->Add(mx);
		FillMXListBox();
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::SaveMXData()
{
	TTPRDLLInterface::SaveTPRMX(ID,MX);
	int c = DeletedMX->Count;
   TTPRMX* mx;
	for(int i=0;i<c;i++)
		{
		mx=(TTPRMX*)(DeletedMX->Items[i]);
      TTPRDLLInterface::DeleteTPRMX(ID,mx->ID);
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::ClearMXData()
{
	TTPRMX* mx;
	int c = MX->Count;
	for(int i=0;i<c;i++)
		{
		mx=(TTPRMX*)(MX->Items[i]);
		delete mx;
		}
	MX->Clear();

	c = DeletedMX->Count;
	for(int i=0;i<c;i++)
		{
		mx=(TTPRMX*)(DeletedMX->Items[i]);
		delete mx;
		}
	DeletedMX->Clear();
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::FillMXData()
{
	if( (ID<=0) || (TprRoot==0) ) return;
	ClearMXData();
	TTPRDLLInterface::LoadTPRMX(ID,MX,NULL);
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::ViewButtonClick(TObject *Sender)
{
	TTPRMX* mx;
	int c=MXListBox->ItemIndex;
	if(c==-1) return;
	mx=(TTPRMX*) (MX->Items[c]);
	if(mx==NULL) return;
   TPRMXForm->ViewMX(mx);
}
//---------------------------------------------------------------------------
void __fastcall TTPRForm::MXListBoxDrawItem(TWinControl *Control,
      int Index, TRect &Rect, TOwnerDrawState State)
{
	TTPRMX* mx;
   mx = (TTPRMX*)(MX->Items[Index]);
   if(State.Contains(odSelected))
      MXListBox->Canvas->Brush->Color=clSkyBlue;
   else
      MXListBox->Canvas->Brush->Color=clWhite;
   MXListBox->Canvas->FillRect(Rect);
   AnsiString s="���� �����������: ";
   s+=mx->Date.DateString();
   if(mx->ValidNow)
      MXListBox->Canvas->Font->Style=TFontStyles()<< fsBold;
   else
      MXListBox->Canvas->Font->Style=TFontStyles();
   MXListBox->Canvas->TextOut(Rect.left+2,Rect.top+2,s);
   s="��������: ";
   s+=mx->Description;
   MXListBox->Canvas->TextOut(Rect.left+2,Rect.top+2+14,s);
}
//---------------------------------------------------------------------------

