//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "TPRPointForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TTPRPointForm *TPRPointForm;
//---------------------------------------------------------------------------
__fastcall TTPRPointForm::TTPRPointForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall TTPRPointForm::fEditChange(TObject *Sender)
{
   fEdit->Font->Color=clBlue;
   KEdit->Font->Color=clBlue;
   QEdit->Font->Color=clBlue;
   double f,K,Q;
   try
      {
      fEdit->Text.ToDouble();
      }
   catch(EConvertError& e)
      {
      fEdit->Font->Color=clRed;
      };
   try
      {
      KEdit->Text.ToDouble();
      }
   catch(EConvertError& e)
      {
      KEdit->Font->Color=clRed;
      };
   try
      {
      QEdit->Text.ToDouble();
      }
   catch(EConvertError& e)
      {
      QEdit->Font->Color=clRed;
      };
}
//---------------------------------------------------------------------------
bool __fastcall TTPRPointForm::AddPoint(double &f, double &K, double &Q)
{
   KEdit->Text="";
   fEdit->Text="";
   QEdit->Text="";
   Caption="���������� �����";
   ActiveControl=fEdit;

   bool Ok= ShowModal()==mrOk;
   if(Ok)
      {//Save changes
      try
         {
         double ff,KK,QQ;
         ff=fEdit->Text.ToDouble();
         KK=KEdit->Text.ToDouble();
         QQ=QEdit->Text.ToDouble();
         f=ff;
         K=KK;
         Q=QQ;
         }
      catch(...)
         {
         Ok=false;
         };
      }//if
   return Ok;

}
//---------------------------------------------------------------------------
bool __fastcall TTPRPointForm::EditPoint(double &f, double &K, double &Q)
{
   KEdit->Text=K;
   fEdit->Text=f;
   QEdit->Text=Q;

   Caption="�������������� �����";
   ActiveControl=fEdit;
   bool Ok= ShowModal()==mrOk;
   if(Ok)
      {//Save changes
      try
         {
         double ff,KK,QQ;
         ff=fEdit->Text.ToDouble();
         KK=KEdit->Text.ToDouble();
         QQ=QEdit->Text.ToDouble();
         f=ff;
         K=KK;
         Q=QQ;
         }
      catch(...)
         {
         Ok=false;
         };
      }//if
   return Ok;
}
//---------------------------------------------------------------------------

