//---------------------------------------------------------------------------

#ifndef TPRRangeForm_unitH
#define TPRRangeForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TTPRRangeForm : public TForm
{
__published:	// IDE-managed Components
   TGroupBox *GroupBox1;
   TLabel *Label1;
   TLabel *Label2;
   TLabel *Label3;
   TLabel *f1Label;
   TLabel *Q1Label;
   TLabel *K1Label;
   TGroupBox *GroupBox2;
   TLabel *Label4;
   TLabel *Label5;
   TLabel *Label6;
   TLabel *f2Label;
   TLabel *Q2Label;
   TLabel *K2Label;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
   TLabel *Label7;
   TBitBtn *CalculateButton;
   TNumberEdit *KEdit;
   void __fastcall CalculateButtonClick(TObject *Sender);
private:	// User declarations
   double Kd;
public:		// User declarations
   __fastcall TTPRRangeForm(TComponent* Owner);
   bool __fastcall Edit(TTPRMX* mx, int RangeIndex);
};
//---------------------------------------------------------------------------
extern PACKAGE TTPRRangeForm *TPRRangeForm;
//---------------------------------------------------------------------------
#endif
