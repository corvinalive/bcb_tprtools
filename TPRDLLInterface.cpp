//---------------------------------------------------------------------------
#include <vcl.h>
#include <ole2.h>
#pragma hdrstop

#include "TPRDLLInterface.h"
#include "TPRSelectForm_unit.h"

#pragma package(smart_init)

void TTPRDLLInterface::SelectTPR(TComponent* Owner,IStorage* RootStorage, TTPR* TPR)
{//+
	if(!TPR)  return;
	OpenRootStorage(RootStorage);
	if(!TprRoot) return;
	TTPRSelectForm* SelectForm=new TTPRSelectForm(Owner);
	SelectForm->SelectTPR(TPR);
	delete SelectForm;
	TprRoot->Release();
	return;
}
//---------------------------------------------------------------------------
void TTPRDLLInterface::ManageTPR(TComponent* Owner, IStorage* RootStorage)
{
	OpenRootStorage(RootStorage);
	if(!TprRoot) return;
	TTPRSelectForm* SelectForm=new TTPRSelectForm(Owner);
	SelectForm->Manage();
	delete SelectForm;
	TprRoot->Release();
}
//---------------------------------------------------------------------------
void TTPRDLLInterface::ViewTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR)
{
	OpenRootStorage(RootStorage);
	if(!TprRoot) return;
	TTPRForm* MMForm=new TTPRForm(Owner);
	MMForm->ViewTPR(TPR);
	delete MMForm;
	TprRoot->Release();
}
//---------------------------------------------------------------------------
bool TTPRDLLInterface::EditTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR)
{
	OpenRootStorage(RootStorage);
	if(!TprRoot) return false;
	TTPRForm* MMForm=new TTPRForm(Owner);
	bool res=MMForm->EditTPR(TPR);
	delete MMForm;
	TprRoot->Release();
	return res;
}
//---------------------------------------------------------------------------
void TTPRDLLInterface::GetTPR(/*in*/IStorage* RootStorage,/*in*/int ID,/*out*/TTPR* TPR)
{
   if(TPR==0) return;
   if(ID==0) return;
	OpenRootStorage(RootStorage);
   GetTPRFromID(ID,TPR);
	if(!TprRoot) return;
	TprRoot->Release();
}
//---------------------------------------------------------------------------
void TTPRDLLInterface::SelectTPRwithMXList(/*in*/TComponent* Owner,
   /*in*/IStorage* RootStorage,/*out*/TTPR* TPR, TList* MX, TOnManageEvent OnManage)
{
   if(TPR==NULL) return;
   if(MX==NULL) return;

	OpenRootStorage(RootStorage);
	if(TprRoot==NULL) return;
	TTPRSelectForm* SelectForm=new TTPRSelectForm(Owner);
	SelectForm->SelectTPR(TPR);
	delete SelectForm;
   //Changes: fill MX list
   if(TPR->ID!=0)
      {
      LoadTPRMX(TPR->ID,MX, OnManage);
      }
	TprRoot->Release();
	return;
}
//---------------------------------------------------------------------------
void TTPRDLLInterface::GetTPRnMXList_(IStorage* RootStorage, int TPR_ID,
                                                      TTPR* TPR,TList* MX,TOnManageEvent OnManage)
{
   if(TPR==NULL) return;
   if(MX==NULL) return;
   if(TPR_ID==0) return;

	OpenRootStorage(RootStorage);
	if(TprRoot==NULL) return;
   GetTPRnMXList(TPR_ID,TPR,MX,OnManage);
	TprRoot->Release();
	return;

}
//---------------------------------------------------------------------------






//---------------------------------------------------------------------------
__fastcall TTPRDLLInterface::TTPRDLLInterface()
{

}
//---------------------------------------------------------------------------
bool __fastcall TTPRDLLInterface::Edit(TComponent* Owner, IStorage* RootStorage, TTPR* TPR)
{
   return EditTPR(Owner,RootStorage,TPR);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::View(TComponent* Owner, IStorage* RootStorage, TTPR* TPR)
{
   ViewTPR(Owner,RootStorage,TPR);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::Manage(TComponent* Owner, IStorage* RootStorage)
{
   ManageTPR(Owner,RootStorage);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::Select(TComponent* Owner, IStorage* RootStorage, TTPR* TPR)
{
   SelectTPR(Owner,RootStorage,TPR);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::Get(IStorage* RootStorage, int ID, TTPR* TPR)
{
   GetTPR(RootStorage,ID,TPR);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
void* __fastcall TTPRDLLInterface::ManageTPRMX(TActionManage Action, void* Data, int Reserved)
{
   if(Action==New)
      {
      return new TTPRMX();
      }
   if(Action==Delete)
      {
      delete (TTPRMX*) Data;
      return NULL;
      }
   return NULL;
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::SelectwithMXList(TComponent* Owner,
                              IStorage* RootStorage, TTPR* TPR, TList* MXList)
{
   SelectTPRwithMXList(Owner,RootStorage,TPR,MXList,&ManageTPRMX);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::GetTPRnMXList(IStorage* RootStorage, int TPR_ID, TTPR* TPR,TList* MXList)
{
   GetTPRnMXList_(RootStorage,TPR_ID,TPR,MXList,&ManageTPRMX);
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
int __fastcall TTPRDLLInterface::AddMX(IStorage* RootStorage, int TPR_ID, TTPRMX* MX)
{
   //TODO: Add your source code here
   return 0;
}
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------



IStorage* TprRoot;
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::OpenRootStorage(/*in*/IStorage* RootStorage)
{
	TprRoot=0;
	HRESULT res = RootStorage->OpenStorage(WideString("TPRs"),0,OF_READWRITE|STGM_SHARE_EXCLUSIVE,
												0,0,&TprRoot);
	if(res!=S_OK)
		if(res==STG_E_FILENOTFOUND)
			RootStorage->CreateStorage(WideString("TPRs"),OF_READWRITE|STGM_SHARE_EXCLUSIVE,
											0,0,&TprRoot);
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::LoadTPRs(/*in*/TList* TPRs)
{
	IEnumSTATSTG* Enum;
	HRESULT res = TprRoot->EnumElements(0,0,0,&Enum);
	if(res!=S_OK) return;
	Enum->Reset();
	STATSTG st;
	IMalloc* m;
	res = CoGetMalloc(1,&m);
	if(res!=S_OK)
		{
		Enum->Release();
		return;
		}
	st.pwcsName =(wchar_t*) m->Alloc(500);
	while(Enum->Next(1,&st,0)==S_OK)
		{
		IStorage* stor;
		res = TprRoot->OpenStorage(st.pwcsName,0,OF_READ|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK) continue;

		IStream* Stream;
		//try to open Stream
		HRESULT res=stor->OpenStream(WideString("TPR"),0,
			OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			stor->Release();
			continue;
			}
		//load from stream
		TTPR* p=new TTPR;
		ULONG lg;
		res = Stream->Read(p,sizeof(TTPR),&lg);
		if(res!=S_OK)
			delete p;
		else
			TPRs->Add(p);
		Stream->Release();
		stor->Release();
		}
	m->Free(st.pwcsName);
	m->Release();
	Enum->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::SaveTPRs(/*in*/TList* TPRs)
{
	if( (!TprRoot) || (!TPRs) ) return;
	HRESULT res;
	TTPR* p;
	IStorage* stor;
	IStream* Stream;
	int c=TPRs->Count;
	for(int i=0;i<c;i++)
		{
		p = (TTPR*)(TPRs->Items[i]);
		if(!p) continue;
		WideString ws(p->ID);
		res = TprRoot->OpenStorage(ws,0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK)
			{//If error open storage, try to create
			res = TprRoot->CreateStorage(ws,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
			if(res!=S_OK)
				continue;
			}

		//try to open Stream
		res=stor->OpenStream(WideString("TPR"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			res = stor->CreateStream(WideString("TPR"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
			if(res!=S_OK)
				{
				stor->Release();
				continue;
				}
			}
		ULONG ul;
		Stream->Write(p,sizeof(TTPR),&ul);
		Stream->Release();
		stor->Release();
		}
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::SaveTPR(TTPR* TPR)
{
	if( (!TprRoot) || (!TPR) ) return;
	HRESULT res;
	IStorage* stor;
	IStream* Stream;
	if(TPR->ID==0) TPR->ID=rand();
	WideString ws(TPR->ID);
	res = TprRoot->OpenStorage(ws,0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = TprRoot->CreateStorage(ws,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&stor);
		if(res!=S_OK)
			return;
		}

	//try to open Stream
	res=stor->OpenStream(WideString("TPR"),0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		{
		res = stor->CreateStream(WideString("TPR"),OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
		if(res!=S_OK)
			{
			stor->Release();
			return;
			}
		}
	ULONG ul;
	Stream->Write(TPR,sizeof(TTPR),&ul);
	Stream->Release();
	stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::DeleteTPR(/*in*/int ID)
{
	TprRoot->DestroyElement(WideString(ID));
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::GetTPRFromID(/*in*/int ID,/*out*/TTPR* TPR)
{
   if((!ID) || (!TPR) ) return;
   memset(TPR,0,sizeof(TTPR));
	IStorage* stor;
	HRESULT res = TprRoot->OpenStorage(WideString(ID),0,OF_READ|OF_SHARE_EXCLUSIVE,0,0,&stor);
	if(res!=S_OK) return;
	IStream* Stream;
	//try to open Stream
	res=stor->OpenStream(WideString("TPR"),0,OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
	if(res!=S_OK)
		{
		stor->Release();
		return;
		}
	//load from stream
	ULONG lg;
	res = Stream->Read(TPR,sizeof(TTPR),&lg);
	Stream->Release();
	stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::LoadTPRMX(int TPR_ID,/*in*/TList* TPRMXList,TOnManageEvent OnManage)
{
//if OnManage==NULL then list used only in DLL
   if(TPR_ID==0) return;
   if(TprRoot==NULL) return;
   if(TPRMXList==NULL) return;

	IStorage* tpr_stor;
	HRESULT res = TprRoot->OpenStorage(WideString(TPR_ID),0,OF_READ|OF_SHARE_EXCLUSIVE,0,0,&tpr_stor);
	if(res!=S_OK) return;

   IStorage* mx_stor=0;
	//try to open
   res = tpr_stor->OpenStorage(WideString("TPR_MX"),0,OF_READ|OF_SHARE_EXCLUSIVE,0,0,&mx_stor);

	if(res!=S_OK)
		{//MXCount=0
		tpr_stor->Release();
		return;
		}
   //Get count of streams
	IEnumSTATSTG* Enum;
	res = mx_stor->EnumElements(0,0,0,&Enum);
	if(res!=S_OK) return;
	Enum->Reset();
	STATSTG st;
	IMalloc* m;
	res = CoGetMalloc(1,&m);
	if(res!=S_OK)
		{
		Enum->Release();
      mx_stor->Release();
      tpr_stor->Release();
		return;
		}
	st.pwcsName =(wchar_t*) m->Alloc(500);
	while(Enum->Next(1,&st,0)==S_OK)
		{
		IStream* Stream;
		//try to open Stream
		HRESULT res=mx_stor->OpenStream(st.pwcsName,0,
			OF_READ|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			continue;

		//load from stream
//Changed: 29/01/2007
      if(OnManage==NULL)
         {//Local version
   		TTPRMX* p=new TTPRMX;
   		ULONG lg;
   		res = Stream->Read(p,sizeof(TTPRMX),&lg);
   		if(res!=S_OK)
   			delete p;
   		else
   			TPRMXList->Add(p);
   		Stream->Release();
         }
      else
         {//list used an another module
   		TTPRMX* p=(TTPRMX*) OnManage(New,NULL,0);
         ULONG lg;
		   res = Stream->Read(p,sizeof(TTPRMX),&lg);
   		if(res!=S_OK)
            {
		   	OnManage(Delete,p,0);
            }
   		else
	   		TPRMXList->Add(p);
		   Stream->Release();
         }//end else if
		}//end while
	m->Free(st.pwcsName);
	m->Release();
	Enum->Release();
   mx_stor->Release();
   tpr_stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::SaveTPRMX(/*in*/int TPR_ID,/*in*/ TList* TPRMXList)
{
	if( (TprRoot==NULL) || (TPR_ID==0) ||(TPRMXList==0) ) return;
   if( TPRMXList->Count==0) return;

	IStorage* tpr_stor;
	IStorage* mx_stor;
	HRESULT res = TprRoot->OpenStorage(WideString(TPR_ID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&tpr_stor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = TprRoot->CreateStorage(WideString(TPR_ID),OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&tpr_stor);
		if(res!=S_OK)
			return;
		}
	res = tpr_stor->OpenStorage(WideString("TPR_MX"),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mx_stor);
	if(res!=S_OK)
		{//If error open storage, try to create
		res = tpr_stor->CreateStorage(WideString("TPR_MX"),OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mx_stor);
		if(res!=S_OK)
			{
			tpr_stor->Release();
			return;
			}
		}

	TTPRMX* mx;
	IStream* Stream;
	int c=TPRMXList->Count;
	for(int i=0;i<c;i++)
		{
		mx = (TTPRMX*)(TPRMXList->Items[i]);
		if(!mx) continue;
		WideString ws(mx->ID);
		res=mx_stor->OpenStream(ws,0,OF_WRITE|STGM_SHARE_EXCLUSIVE,0, &Stream);
		if(res!=S_OK)
			{
			res = mx_stor->CreateStream(ws,OF_WRITE|STGM_SHARE_EXCLUSIVE,0,0,&Stream);
			if(res!=S_OK)
				{
				tpr_stor->Release();
				continue;
				}
			}
		ULONG ul;
		Stream->Write(mx,sizeof(TTPRMX),&ul);
		Stream->Release();
		}
	mx_stor->Release();
	tpr_stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::DeleteTPRMX(/*in*/int TPR_ID,/*in*/int MX_ID)
{
	if( (TprRoot==0) || (TPR_ID==0) ||(MX_ID==0) ) return;
	IStorage* tpr_stor;
	IStorage* mx_stor;
	HRESULT res = TprRoot->OpenStorage(WideString(TPR_ID),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&tpr_stor);
	if(res!=S_OK) return;
	res = tpr_stor->OpenStorage(WideString("TPR_MX"),0,OF_READWRITE|OF_SHARE_EXCLUSIVE,0,0,&mx_stor);
	if(res!=S_OK)
		{
		tpr_stor->Release();
		return;
		}
	mx_stor->DestroyElement(WideString(TPR_ID));
	mx_stor->Release();
	tpr_stor->Release();
}
//---------------------------------------------------------------------------
void __fastcall TTPRDLLInterface::GetTPRnMXList(int ID,TTPR* TPR,TList* MXList,TOnManageEvent Manage)
{
   GetTPRFromID(ID, TPR);
   LoadTPRMX(ID,MXList,Manage);
}
//---------------------------------------------------------------------------






