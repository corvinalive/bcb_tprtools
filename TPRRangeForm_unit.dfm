object TPRRangeForm: TTPRRangeForm
  Left = 390
  Top = 237
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1044#1072#1085#1085#1099#1077' '#1087#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1072
  ClientHeight = 312
  ClientWidth = 246
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Courier'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label7: TLabel
    Left = 8
    Top = 184
    Width = 232
    Height = 26
    Caption = #1050#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1087#1088#1077#1086#1073#1088#1072#1079#1086#1074#1072#1085#1080#1103' '#1074' '#1087#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1077', '#1080#1084#1087'/'#1084'3:'
    WordWrap = True
  end
  object GroupBox1: TGroupBox
    Left = 8
    Top = 8
    Width = 233
    Height = 81
    Caption = #1044#1072#1085#1085#1099#1077' '#1090#1086#1095#1082#1080' 1'
    TabOrder = 0
    object Label1: TLabel
      Left = 8
      Top = 24
      Width = 104
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' f, '#1043#1094
    end
    object Label2: TLabel
      Left = 8
      Top = 40
      Width = 112
      Height = 13
      Caption = #1056#1072#1089#1093#1086#1076' Q, '#1084'3/'#1095
    end
    object Label3: TLabel
      Left = 8
      Top = 56
      Width = 72
      Height = 13
      Caption = #1050', '#1080#1084#1087'/'#1084'3'
    end
    object f1Label: TLabel
      Left = 136
      Top = 24
      Width = 56
      Height = 13
      Caption = 'f1Label'
    end
    object Q1Label: TLabel
      Left = 136
      Top = 40
      Width = 56
      Height = 13
      Caption = 'Q1Label'
    end
    object K1Label: TLabel
      Left = 136
      Top = 56
      Width = 56
      Height = 13
      Caption = 'K1Label'
    end
  end
  object GroupBox2: TGroupBox
    Left = 8
    Top = 96
    Width = 233
    Height = 81
    Caption = #1044#1072#1085#1085#1099#1077' '#1090#1086#1095#1082#1080' 2'
    TabOrder = 1
    object Label4: TLabel
      Left = 8
      Top = 24
      Width = 104
      Height = 13
      Caption = #1063#1072#1089#1090#1086#1090#1072' f, '#1043#1094
    end
    object Label5: TLabel
      Left = 8
      Top = 40
      Width = 112
      Height = 13
      Caption = #1056#1072#1089#1093#1086#1076' Q, '#1084'3/'#1095
    end
    object Label6: TLabel
      Left = 8
      Top = 56
      Width = 72
      Height = 13
      Caption = #1050', '#1080#1084#1087'/'#1084'3'
    end
    object f2Label: TLabel
      Left = 136
      Top = 24
      Width = 56
      Height = 13
      Caption = 'f2Label'
    end
    object Q2Label: TLabel
      Left = 136
      Top = 40
      Width = 56
      Height = 13
      Caption = 'Q2Label'
    end
    object K2Label: TLabel
      Left = 136
      Top = 56
      Width = 56
      Height = 13
      Caption = 'K2Label'
    end
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 280
    Width = 113
    Height = 25
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 4
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 128
    Top = 280
    Width = 113
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 5
    Kind = bkCancel
  end
  object CalculateButton: TBitBtn
    Left = 8
    Top = 248
    Width = 233
    Height = 25
    Caption = #1056#1072#1089#1095#1080#1090#1072#1090#1100
    TabOrder = 3
    OnClick = CalculateButtonClick
  end
  object KEdit: TNumberEdit
    Left = 8
    Top = 216
    Width = 233
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    NumberType = Double
  end
end
