//---------------------------------------------------------------------------
#include <vcl.h>
#include <windows.h>
#pragma hdrstop
#include "tpr.h"
#include "tpr_unit.h"
#include "selectform_unit.h"

#pragma argsused
//---------------------------------------------------------------------------
int WINAPI DllEntryPoint(HINSTANCE hinst, unsigned long reason, void* lpReserved)
{
	return 1;
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void SelectTPR(TComponent* Owner,IStorage* RootStorage,
															TTPR* TPR)
{//+
	if(!TPR)  return;
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectForm* SelectForm=new TSelectForm(Owner);
	SelectForm->SelectTPR(TPR);
	delete SelectForm;
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void ManageTPR(TComponent* Owner, IStorage* RootStorage)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TSelectForm* SelectForm=new TSelectForm(Owner);
	SelectForm->Manage();
	delete SelectForm;
	Root->Release();
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void ViewTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR)
{
	OpenRootStorage(RootStorage);
	if(!Root) return;
	TTPRForm* MMForm=new TTPRForm(Owner);
	MMForm->ViewTPR(TPR);
	delete MMForm;
	Root->Release();
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) bool EditTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR)
{
	OpenRootStorage(RootStorage);
	if(!Root) return false;
	TTPRForm* MMForm=new TTPRForm(Owner);
	bool res=MMForm->EditTPR(TPR);
	delete MMForm;
	Root->Release();
	return res;
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void GetTPR(/*in*/IStorage* RootStorage,/*in*/int ID,/*out*/TTPR* TPR)
{
   if(TPR==0) return;
   if(ID==0) return;
	OpenRootStorage(RootStorage);
   GetTPRFromID(ID,TPR);
	if(!Root) return;
	Root->Release();
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void SelectTPRwithMXList(/*in*/TComponent* Owner,
   /*in*/IStorage* RootStorage,/*out*/TTPR* TPR, TList* MX, TOnManageEvent OnManage)
{
   if(TPR==NULL) return;
   if(MX==NULL) return;

	OpenRootStorage(RootStorage);
	if(Root==NULL) return;
	TSelectForm* SelectForm=new TSelectForm(Owner);
	SelectForm->SelectTPR(TPR);
	delete SelectForm;
   //Changes: fill MX list
   if(TPR->ID!=0)
      {
      LoadTPRMX(TPR->ID,MX, OnManage);
      }
	Root->Release();
	return;
}
//---------------------------------------------------------------------------
extern "C" __declspec(dllexport) void GetTPRnMXList(IStorage* RootStorage, int TPR_ID,
                                                      TTPR* TPR,TList* MX,TOnManageEvent OnManage)
{
   if(TPR==NULL) return;
   if(MX==NULL) return;
   if(TPR_ID==0) return;

	OpenRootStorage(RootStorage);
	if(Root==NULL) return;
   GetTPRnMXList(TPR_ID,TPR,MX,OnManage);
	Root->Release();
	return;

}
//---------------------------------------------------------------------------

