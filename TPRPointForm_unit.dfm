object TPRPointForm: TTPRPointForm
  Left = 357
  Top = 330
  Width = 402
  Height = 115
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1090#1086#1095#1082#1080
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 4
    Width = 75
    Height = 16
    Caption = #1063#1072#1089#1090#1086#1090#1072', '#1043#1094
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label2: TLabel
    Left = 264
    Top = 4
    Width = 108
    Height = 16
    Caption = #1050#1086#1101#1092'.-'#1085#1090', '#1080#1084#1087'/'#1084'3'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object Label3: TLabel
    Left = 133
    Top = 4
    Width = 80
    Height = 16
    Caption = #1056#1072#1089#1093#1086#1076', '#1084'3/'#1095
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = []
    ParentFont = False
  end
  object fEdit: TNumberEdit
    Left = 8
    Top = 24
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
    Text = '0'
    OnChange = fEditChange
    NumberType = Double
  end
  object QEdit: TNumberEdit
    Left = 134
    Top = 24
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 1
    Text = '0'
    OnChange = fEditChange
    NumberType = Double
  end
  object BitBtn1: TBitBtn
    Left = 8
    Top = 54
    Width = 185
    Height = 25
    TabOrder = 3
    Kind = bkOK
  end
  object BitBtn2: TBitBtn
    Left = 196
    Top = 54
    Width = 184
    Height = 25
    Caption = #1054#1090#1084#1077#1085#1072
    TabOrder = 4
    Kind = bkCancel
  end
  object KEdit: TNumberEdit
    Left = 259
    Top = 24
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clBlue
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    OnChange = fEditChange
    NumberType = Double
  end
end
