object Form1: TForm1
  Left = 278
  Top = 103
  Width = 696
  Height = 480
  Caption = 'Form1'
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Button1: TButton
    Left = 92
    Top = 160
    Width = 75
    Height = 25
    Caption = 'Select'
    TabOrder = 0
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 186
    Top = 164
    Width = 75
    Height = 25
    Caption = 'Manage'
    TabOrder = 1
    OnClick = Button2Click
  end
  object Button3: TButton
    Left = 270
    Top = 166
    Width = 75
    Height = 25
    Caption = 'View'
    TabOrder = 2
    OnClick = Button3Click
  end
  object Button4: TButton
    Left = 366
    Top = 168
    Width = 75
    Height = 27
    Caption = 'Edit'
    TabOrder = 3
    OnClick = Button4Click
  end
  object Button5: TButton
    Left = 328
    Top = 272
    Width = 209
    Height = 25
    Caption = 'Get with List'
    TabOrder = 4
    OnClick = Button5Click
  end
end
