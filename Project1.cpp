//---------------------------------------------------------------------------

#include <vcl.h>
#include "unit1.h"
#pragma hdrstop
//---------------------------------------------------------------------------
USEFORM("Unit1.cpp", Form1);
USEFORM("TPRSelectForm_unit.cpp", TPRSelectForm);
USEFORM("TPRForm_unit.cpp", TPRForm);
USEFORM("TPRMXForm_unit.cpp", TPRMXForm);
USEFORM("TPRPointForm_unit.cpp", TPRPointForm);
USEFORM("TPRRangeForm_unit.cpp", TPRRangeForm);
//---------------------------------------------------------------------------
WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	try
	{
		Application->Initialize();
   	Application->CreateForm(__classid(TForm1), &Form1);
       Application->CreateForm(__classid(TTPRMXForm), &TPRMXForm);
       Application->CreateForm(__classid(TTPRPointForm), &TPRPointForm);
       Application->CreateForm(__classid(TTPRRangeForm), &TPRRangeForm);
       Application->Run();
	}
	catch (Exception &exception)
	{
		Application->ShowException(&exception);
	}
	catch (...)
	{
		try
		{
			throw Exception("");
		}
		catch (Exception &exception)
		{
			Application->ShowException(&exception);
		}
	}
	return 0;
}
//---------------------------------------------------------------------------
