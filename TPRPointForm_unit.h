//---------------------------------------------------------------------------

#ifndef TPRPointForm_unitH
#define TPRPointForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TTPRPointForm : public TForm
{
__published:	// IDE-managed Components
   TNumberEdit *fEdit;
   TNumberEdit *QEdit;
   TBitBtn *BitBtn1;
   TBitBtn *BitBtn2;
   TLabel *Label1;
   TLabel *Label2;
   TLabel *Label3;
   TNumberEdit *KEdit;
   void __fastcall fEditChange(TObject *Sender);
private:	// User declarations
   bool ArgumentIsFrequensy;
public:		// User declarations
   __fastcall TTPRPointForm(TComponent* Owner);
   bool __fastcall AddPoint(double &f, double &K, double &Q);
   bool __fastcall EditPoint(double &f, double &K, double &Q);
};
//---------------------------------------------------------------------------
extern PACKAGE TTPRPointForm *TPRPointForm;
//---------------------------------------------------------------------------
#endif
