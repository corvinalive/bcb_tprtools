object TPRMXForm: TTPRMXForm
  Left = 250
  Top = 126
  BorderIcons = [biSystemMenu]
  BorderStyle = bsDialog
  Caption = #1044#1086#1073#1072#1074#1083#1077#1085#1080#1077' '#1076#1072#1085#1085#1099#1093' '#1086' '#1087#1086#1074#1077#1088#1082#1077
  ClientHeight = 456
  ClientWidth = 647
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'MS Sans Serif'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 292
    Top = 65
    Width = 138
    Height = 13
    Caption = #1042#1103#1079#1082#1086#1089#1090#1100' '#1087#1088#1080' '#1087#1086#1074#1077#1088#1082#1077', '#1089#1057#1090
  end
  object Label1: TLabel
    Left = 442
    Top = 10
    Width = 71
    Height = 13
    Caption = #1044#1072#1090#1072' '#1087#1086#1074#1077#1088#1082#1080
  end
  object Label4: TLabel
    Left = 310
    Top = 358
    Width = 27
    Height = 16
    Caption = #1050#1076'='
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'MS Sans Serif'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object MXTypeRadioGroup: TRadioGroup
    Left = 2
    Top = 2
    Width = 285
    Height = 105
    Caption = #1042#1080#1076' '#1043#1061
    ItemIndex = 0
    Items.Strings = (
      #1055#1086#1089#1090#1086#1103#1085#1085#1099#1081' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090' '#1074' '#1088#1072#1073#1086#1095#1077#1084' '#1076#1080#1072#1087#1072#1079#1086#1085#1077
      #1055#1086#1089#1090#1086#1103#1085#1085#1099#1077' '#1082#1086#1101#1092#1092#1080#1094#1080#1077#1085#1090#1099' '#1074' '#1087#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1072#1093
      #1050#1091#1089#1086#1095#1085#1086'-'#1083#1080#1085#1077#1081#1085#1072#1103
      #1055#1086#1083#1080#1085#1086#1084#1080#1085#1072#1083#1100#1085#1072#1103)
    TabOrder = 0
  end
  object DatePicker: TDateTimePicker
    Left = 440
    Top = 28
    Width = 123
    Height = 21
    CalAlignment = dtaLeft
    Date = 38760.976596169
    Time = 38760.976596169
    DateFormat = dfShort
    DateMode = dmComboBox
    Kind = dtkDate
    ParseInput = False
    TabOrder = 3
  end
  object OkButton: TBitBtn
    Left = 4
    Top = 428
    Width = 318
    Height = 25
    TabOrder = 9
    Kind = bkOK
  end
  object CancelButton: TBitBtn
    Left = 324
    Top = 428
    Width = 318
    Height = 25
    TabOrder = 10
    Kind = bkCancel
  end
  object ViscosityEdit: TNumberEdit
    Left = 294
    Top = 82
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Text = '0'
    NumberType = Double
  end
  object KEdit: TNumberEdit
    Left = 340
    Top = 354
    Width = 121
    Height = 24
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Courier New'
    Font.Style = []
    ParentFont = False
    TabOrder = 8
    Text = '0'
    NumberType = Double
  end
  object PointGroupBox: TGroupBox
    Left = 2
    Top = 108
    Width = 285
    Height = 190
    Caption = #1044#1072#1085#1085#1099#1077' '#1090#1086#1095#1077#1082' '#1087#1086#1074#1077#1088#1082#1080
    TabOrder = 5
    object SG: TStringGrid
      Left = 6
      Top = 14
      Width = 271
      Height = 143
      Color = clInfoBk
      ColCount = 4
      DefaultRowHeight = 18
      FixedCols = 0
      RowCount = 2
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRowSelect]
      TabOrder = 0
      OnDblClick = EditPointButtonClick
      OnKeyDown = SGKeyDown
      ColWidths = (
        25
        81
        74
        66)
    end
    object AddPointButton: TBitBtn
      Left = 4
      Top = 160
      Width = 92
      Height = 25
      Caption = #1044#1086#1073#1072#1074#1080#1090#1100'...'
      TabOrder = 1
      OnClick = AddPointButtonClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        88888888880000888888888887AAA0888888888887AAA0888888888887AAA088
        8888888887AAA0888888877777AAA00000887AAAAAAAAAAAAA087AAAAAAAAAAA
        AA087AAAAAAAAAAAAA08877777AAA0000088888887AAA0888888888887AAA088
        8888888887AAA0888888888887AAA08888888888887778888888}
    end
    object EditPointButton: TBitBtn
      Left = 96
      Top = 160
      Width = 92
      Height = 25
      Caption = #1048#1079#1084#1077#1085#1080#1090#1100'...'
      TabOrder = 2
      OnClick = EditPointButtonClick
      Glyph.Data = {
        36030000424D3603000000000000360000002800000010000000100000000100
        18000000000000030000120B0000120B00000000000000000000FFFFFFFFFFFF
        FFFFFFFFFFFF0000000000000000000000000000000000000000000000000000
        00000000000000000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
        CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000FFCDB4FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4FFCDB4FF
        CDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF000000FFCDB4FFFFFF0000000000000000FFFFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF000000FFCDB4000000FF
        FFFFFFFFFF000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFFFFFFFF00000000000000FFFF000000FFFFFF000000FFFFFFFFFFFFFFFF
        FFFFFFFFFFCDB4000000FFFFFFFFFFFFFFFFFFFFFFFF00000000FFFF00FFFF00
        0000000000FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4FFCDB4000000FFFFFFFFFFFF
        FFFFFF00000000FFFF00FFFF0000000000000000000000000000000000000000
        00000000000000000000FFFFFFFFFFFF00000000FFFF00FFFF000000000000FF
        8548FF8548FF8548FF8548FF8548FF8548FF8548FF8548000000FFFFFF000000
        FFFFFF00FFFF0000000000000000000000000000000000000000000000000000
        00000000000000000000000000FFFFFF00FFFF000000000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF000000000000
        000000000000FFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFF000000000000FFFFFF000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        000000000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF}
    end
    object DeletePointButton: TBitBtn
      Left = 188
      Top = 160
      Width = 92
      Height = 25
      Caption = #1059#1076#1072#1083#1080#1090#1100'...'
      TabOrder = 3
      OnClick = DeletePointButtonClick
      Glyph.Data = {
        F6000000424DF600000000000000760000002800000010000000100000000100
        0400000000008000000000000000000000001000000000000000000000000000
        8000008000000080800080000000800080008080000080808000C0C0C0000000
        FF0000FF000000FFFF00FF000000FF00FF00FFFF0000FFFFFF00888888888888
        8888888088888888880888000888888888888800008888888088888000888888
        0888888800088880088888888000880088888888880000088888888888800088
        8888888888000008888888888000880088888880000888800888880000888888
        0088880008888888880888888888888888888888888888888888}
    end
  end
  object RangeGroupBox: TGroupBox
    Left = 290
    Top = 108
    Width = 355
    Height = 190
    Caption = #1044#1072#1085#1085#1099#1077' '#1087#1086#1074#1077#1088#1082#1080' '#1074' '#1087#1086#1076#1076#1080#1072#1087#1072#1079#1086#1085#1072#1093
    TabOrder = 6
    object RangeSG: TStringGrid
      Left = 6
      Top = 16
      Width = 345
      Height = 168
      ColCount = 6
      DefaultRowHeight = 16
      FixedCols = 0
      Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goColSizing, goRowSelect]
      TabOrder = 0
      OnDblClick = RangeSGDblClick
      OnKeyDown = RangeSGKeyDown
      ColWidths = (
        31
        56
        53
        64
        64
        64)
    end
  end
  object ArgumentRadioGroup: TRadioGroup
    Left = 290
    Top = 8
    Width = 145
    Height = 57
    Caption = #1040#1088#1075#1091#1084#1077#1085#1090
    ItemIndex = 0
    Items.Strings = (
      #1063#1072#1089#1090#1086#1090#1072' f, '#1043#1094
      #1056#1072#1089#1093#1086#1076' Q, '#1084'3/'#1095)
    TabOrder = 1
    OnClick = ArgumentRadioGroupClick
  end
  object ValidNowCheckBox: TCheckBox
    Left = 440
    Top = 51
    Width = 209
    Height = 17
    Caption = #1044#1077#1081#1089#1090#1074#1080#1090#1077#1083#1100#1085#1099' '#1085#1072' '#1090#1077#1082#1091#1097#1080#1081' '#1084#1086#1084#1077#1085#1090
    TabOrder = 4
  end
  object DescGroupBox: TGroupBox
    Left = 2
    Top = 300
    Width = 291
    Height = 124
    Caption = #1054#1087#1080#1089#1072#1085#1080#1077
    TabOrder = 7
    object DescriptionMemo: TMemo
      Left = 2
      Top = 13
      Width = 285
      Height = 105
      TabOrder = 0
    end
  end
end
