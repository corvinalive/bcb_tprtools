//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "tpr.h"
#include "TPRRangeForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TTPRRangeForm *TPRRangeForm;
//---------------------------------------------------------------------------
__fastcall TTPRRangeForm::TTPRRangeForm(TComponent* Owner)
   : TForm(Owner)
{
}
//---------------------------------------------------------------------------
bool __fastcall TTPRRangeForm::Edit(TTPRMX* mx, int RangeIndex)
{
   if(mx==NULL) return false;
   if(RangeIndex >= mx->PointCount) return false;
   if(RangeIndex < 0) return false;
   if(RangeIndex >= (TTPRMX_MaxPoints-2)) return false;

   f1Label->Caption=mx->Points[RangeIndex].f;
   K1Label->Caption=mx->Points[RangeIndex].K;
   Q1Label->Caption=mx->Points[RangeIndex].Q;

   f2Label->Caption=mx->Points[RangeIndex+1].f;
   Q2Label->Caption=mx->Points[RangeIndex+1].Q;
   K2Label->Caption=mx->Points[RangeIndex+1].K;

   KEdit->Text=mx->Kd[RangeIndex];

   try
      {
      Kd=(mx->Points[RangeIndex].K + mx->Points[RangeIndex+1].K)/2;
      }
   catch(...)
      {
      Kd=0;
      };
   ActiveControl=KEdit;
   if(ShowModal()==mrOk)
      {
      mx->Kd[RangeIndex]=KEdit->Text.ToDouble();
      return true;
      }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TTPRRangeForm::CalculateButtonClick(TObject *Sender)
{
   KEdit->Text=Kd;   
}
//---------------------------------------------------------------------------

