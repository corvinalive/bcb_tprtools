//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "tpr.h"
#include <ole2.h>
#include "TPRDLLInterface.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	Root=NULL;
	OleInitialize(0);
	AnsiString FN("base.dat");
	if (FileExists(FN))
		{//Open
		HRESULT res;
		res=StgOpenStorage(WideString(FN),NULL,OF_READWRITE|STGM_SHARE_EXCLUSIVE,0,0,&Root);
		if(res!=S_OK) throw(Exception("Error open storage file"));
		}
	else
		{//Create
		HRESULT res;
		res=StgCreateDocfile(WideString(FN),STGM_CREATE|STGM_READWRITE|STGM_SHARE_EXCLUSIVE,0,&Root);
		if(res!=S_OK) throw(Exception("Error create storage file"));
		}


   Inter = new TTPRDLLInterface();
}
//---------------------------------------------------------------------------
__fastcall TForm1::~TForm1()
{
   delete Inter;
	Root->Release();
	FreeLibrary(hinstLib);
	OleUninitialize();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender)
{
		Inter->Select(this,Root,&MM);
		AnsiString s=MM.ID;
		Application->MessageBox(s.c_str(),"Ok",0);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button2Click(TObject *Sender)
{
		Inter->Manage(this,Root);

}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
	Inter->View(this,Root,&MM);
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button4Click(TObject *Sender)
{
	bool res=Inter->Edit(this,Root,&MM);
		if(res)
			Application->MessageBox("return value","Ok",0);
		else
			Application->MessageBox("return value","Cancel",0);
		
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button5Click(TObject *Sender)
{
   TList* l=new TList();
   Inter->GetTPRnMXList(Root,MM.ID,&MM,l);
   delete l;
}
//---------------------------------------------------------------------------

