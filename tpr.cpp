#include "tpr.h"
#pragma hdrstop
//---------------------------------------------------------------------------
int ComparePointMXbyFrequency(const void *a, const void *b)
{
//*elem1  < *elem2        fcmp returns an integer < 0
//	*elem1 == *elem2       fcmp returns 0
//	*elem1  > *elem2        fcmp returns an integer > 0
   double f1=((TTPRMX::TPointMX*)a)->f;
   double f2=((TTPRMX::TPointMX*)b)->f;
   if(f1==f2) return 0;
   if((f1==0) && (f2>0)) return 1;
   if((f2==0) && (f1>0)) return -1;
   return f1-f2;
};
//---------------------------------------------------------------------------
int ComparePointMXbyFlowRate(const void *a, const void *b)
{
//*elem1  < *elem2        fcmp returns an integer < 0
//	*elem1 == *elem2       fcmp returns 0
//	*elem1  > *elem2        fcmp returns an integer > 0
   double Q1=((TTPRMX::TPointMX*)a)->Q;
   double Q2=((TTPRMX::TPointMX*)b)->Q;
   if(Q1==Q2) return 0;
   if((Q1==0) && (Q2>0)) return 1;
   if((Q2==0) && (Q1>0)) return -1;
   return Q1-Q2;
};
//---------------------------------------------------------------------------
void __fastcall SortPoints(TTPRMX* MX)
{
   if(MX->ArgumentMX==TTPRMX::Frequency)
      qsort(MX->Points,TTPRMX_MaxPoints,sizeof(TTPRMX::TPointMX),ComparePointMXbyFrequency);
   else
      qsort(MX->Points,TTPRMX_MaxPoints,sizeof(TTPRMX::TPointMX),ComparePointMXbyFlowRate);
}
//---------------------------------------------------------------------------
double __fastcall CalculateK(TTPRMX* MX,double Argument)
{
   int RangeCount;
   if(MX->MXType==TTPRMX::Const)
      return MX->K;

   if(MX->ArgumentMX==TTPRMX::Frequency)
      {
      switch (MX->MXType)
         {
         case TTPRMX::ConstRange:
            RangeCount=MX->PointCount-1;
            //��� �������������
            if(RangeCount<=0)
               return 0;
            //���� ������� ������ ������ ����� 1-�� ������������, �� ���������� �-��
            // 1-�� ������������
            if(Argument < MX->Points[0].f)
               return MX->Kd[0];
            //���� ������� ������ ��������� �����, �� ���������� �-�� ���������� ������������
            if(Argument > MX->Points[MX->PointCount-1].f)
               return MX->Kd[RangeCount-1];
            for(int i=0;i<RangeCount;i++)
               {
               double f1=MX->Points[i].f;
               double f2=MX->Points[i+1].f;
               if( (Argument>f1) && (Argument<f2))
                  {
                  return MX->Kd[i];
                  }
               }
            //������ �� �������
            throw(Exception("Error in functionCalculateK case TTPRMX::ConstRange"));

         case TTPRMX::Line:
            RangeCount=MX->PointCount-1;
            //��� �������������
            if(RangeCount<=0)
               return 0;
            //���� ������� ������ ������ ����� 1-�� ������������, �� ���������� �-��
            // 1-� �����
            if(Argument <= MX->Points[0].f)
               return MX->Points[0].K;
            //���� ������� ������ ��������� �����, �� ���������� �-�� ��������� �����
            if(Argument >= MX->Points[MX->PointCount-1].f)
               return MX->Points[MX->PointCount-1].K;

            for(int i=0;i<RangeCount;i++)
               {
               double f1=MX->Points[i].f;
               double f2=MX->Points[i+1].f;
               if( (Argument>f1) && (Argument<f2))
                  {
                  double K1=MX->Points[i].K;
                  double K2=MX->Points[i+1].K;
                  double kx=(K2-K1)/(f2-f1);
                  double b=K1-kx*f1;
                  double K=Argument*kx+b;
                  return K;
                  }
               }
            //������ �� �������
            throw(Exception("Error in functionCalculateK case TTPRMX::Line"));

         default:
            throw(Exception("Error in functionCalculateK\n�������������� �������, �������� �� ���������� �-��� � ������� ���������, ������������� � �������������, �������-�������� ������������ �� ��������������."));

         }//switcth

      }//if

   if(MX->ArgumentMX==TTPRMX::FlowRate)
      {
      switch (MX->MXType)
         {
         case TTPRMX::ConstRange:
            RangeCount=MX->PointCount-1;
            //��� �������������
            if(RangeCount<=0)
               return 0;
            //���� ������ ������ ������ ����� 1-�� ������������, �� ���������� �-��
            // 1-�� ������������
            if(Argument < MX->Points[0].Q)
               return MX->Kd[0];
            //���� ������ ������ ��������� �����, �� ���������� �-�� ���������� ������������
            if(Argument > MX->Points[MX->PointCount-1].Q)
               return MX->Kd[RangeCount-1];
            for(int i=0;i<RangeCount;i++)
               {
               double f1=MX->Points[i].Q;
               double f2=MX->Points[i+1].Q;
               if( (Argument>f1) && (Argument<f2))
                  {
                  return MX->Kd[i];
                  }
               }
            //������ �� �������
            throw(Exception("Error in functionCalculateK case TTPRMX::ConstRange"));

         case TTPRMX::Line:
            RangeCount=MX->PointCount-1;
            //��� �������������
            if(RangeCount<=0)
               return 0;
            //���� ������� ������ ������ ����� 1-�� ������������, �� ���������� �-��
            // 1-� �����
            if(Argument < MX->Points[0].Q)
               return MX->Points[0].K;
            //���� ������� ������ ��������� �����, �� ���������� �-�� ��������� �����
            if(Argument > MX->Points[MX->PointCount-1].Q)
               return MX->Points[MX->PointCount-1].K;

            for(int i=0;i<RangeCount;i++)
               {
               double f1=MX->Points[i].Q;
               double f2=MX->Points[i+1].Q;
               if( (Argument>f1) && (Argument<f2))
                  {
                  double K1=MX->Points[i].K;
                  double K2=MX->Points[i+1].K;
                  double kx=(K2-K1)/(f2-f1);
                  double b=K1-kx*f1;
                  double K=Argument*kx+b;
                  return K;
                  }
               }
            //������ �� �������
            throw(Exception("Error in functionCalculateK case TTPRMX::Line"));

         default:
            throw(Exception("Error in functionCalculateK\n�������������� �������, �������� �� ���������� �-��� � ������� ���������, ������������� � �������������, �������-�������� ������������ �� ��������������."));

         }//switcth

      }//if
   return -1;
}

