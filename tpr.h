#include <vcl.h>
//#include <ole2.h>
//#include <inifiles.hpp>

#ifndef TPRH
#define TPRH
//---------------------------------------------------------------------------
class TTPR
{
public:
   int version;
   int ID;
   //Type
   char Company[120];
   char Model[120];
   double D;   //��
   double Qmax;
   char UUN[120];
   char Location[120];
   char Owner[120];
   char SN[120];
};
//---------------------------------------------------------------------------
const TTPRMX_MaxPoints=30;
const TTPRMX_DescriptionTextSize=256;
class TTPRMX
{
public:
   int ID;
   int ProtocolID;
   struct TPointMX
      {
      double f;
      double Q;
      double K;
      } Points[TTPRMX_MaxPoints];
   double Kd[TTPRMX_MaxPoints-1];
   double K;
   double Viscosity;
   TDate Date;
   enum TMXType {Const=0, ConstRange=1, Line=2, Poly=3} MXType;
   enum TArgumentMX {Frequency=0, FlowRate=1} ArgumentMX;

   char Description[TTPRMX_DescriptionTextSize+1];
   bool ValidNow;
   int PointCount;
private:
};
void __fastcall SortPoints(TTPRMX* MX);
double __fastcall CalculateK(TTPRMX* MX,double Argument);
//---------------------------------------------------------------------------
enum TActionManage{New,Delete};
typedef void* __fastcall (__closure *TOnManageEvent)(TActionManage Action,
                                                      void* Data, int Reserved);
#ifndef _DLL



#endif //_DLL
//---------------------------------------------------------------------------

#endif //ProversH

