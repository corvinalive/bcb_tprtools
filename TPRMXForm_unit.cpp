//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "tpr.h"
#include "TPRMXForm_unit.h"
#include "TPRPointForm_unit.h"
#include "TPRRangeForm_unit.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma link "NumberEdit"
#pragma resource "*.dfm"
TTPRMXForm *TPRMXForm;
//---------------------------------------------------------------------------
__fastcall TTPRMXForm::TTPRMXForm(TComponent* Owner)
   : TForm(Owner)
{
   //PointForm=new TPointForm(this);
   //RangeForm=new TRangeForm(this);
   SG->Cells[0][0]="�";
   SG->Cells[1][0]="f (��)";
   SG->Cells[2][0]="Q (�3/�)";
   SG->Cells[3][0]="K (���/�3)";

   RangeSG->Cells[0][0]="�";
   RangeSG->Cells[1][0]="f1, ��";
   RangeSG->Cells[2][0]="Q1, �3/�";
   RangeSG->Cells[3][0]="f2, ��";
   RangeSG->Cells[4][0]="Q2, �3/�";
   RangeSG->Cells[5][0]="���, ���/3";
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::AddPointButtonClick(TObject *Sender)
{
  double f,K,Q;
  //
   if(temp_mx.PointCount==TTPRMX_MaxPoints)
      {
      Application->MessageBox("����� ������� �����","������",MB_ICONSTOP);
      return;
      }
   if(TPRPointForm->AddPoint(f,K,Q))
      {
      temp_mx.Points[temp_mx.PointCount].f=f;
      temp_mx.Points[temp_mx.PointCount].K=K;
      temp_mx.Points[temp_mx.PointCount].Q=Q;
      temp_mx.PointCount++;
      SortPoints(&temp_mx);
      MXtoForm(true);
      }
}
//---------------------------------------------------------------------------
__fastcall TTPRMXForm::~TTPRMXForm()
{
   //delete PointForm;
   //delete RangeForm;
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::ViewMX(TTPRMX* mx)
{
   memcpy(&temp_mx,mx,sizeof(TTPRMX));
   EnableControls(false);
   MXtoForm();
   EnableControls(true);
	Caption="�������� ������ ���";
	OkButton->Caption="�������";
	CancelButton->Visible=false;
   ShowModal();
}
//---------------------------------------------------------------------------
bool __fastcall TTPRMXForm::EditMX(TTPRMX* mx)
{
   memcpy(&temp_mx,mx,sizeof(TTPRMX));
   EnableControls(false);
	Caption="�������������� ������ �������������� ��������������";
	OkButton->Caption="��������� ��������� � �����";
	CancelButton->Visible=true;
	CancelButton->Caption="����� ��� ���������� ���������";
   MXtoForm();
   if(ShowModal()==mrOk)
      {
      FormtoMX();
      memcpy(mx,&temp_mx,sizeof(TTPRMX));
      return true;
      }
   return false;
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::MXtoForm(bool OnlyPoints)
{
   if(temp_mx.PointCount==0)
      {
      SG->RowCount=2;
      SG->Cells[0][1]="";
      SG->Cells[1][1]="";
      SG->Cells[2][1]="";
      SG->Cells[3][1]="";

      RangeSG->RowCount=2;
      RangeSG->Cells[0][1]="";
      RangeSG->Cells[1][1]="";
      RangeSG->Cells[2][1]="";
      RangeSG->Cells[3][1]="";
      RangeSG->Cells[4][1]="";
      RangeSG->Cells[5][1]="";
      }
   else
      {
      SG->RowCount=temp_mx.PointCount+1;
      for(int i=0;i<temp_mx.PointCount;i++)
         {
         SG->Cells[0][i+1]=i+1;
         SG->Cells[1][i+1]=temp_mx.Points[i].f;
         SG->Cells[2][i+1]=temp_mx.Points[i].Q;
         SG->Cells[3][i+1]=temp_mx.Points[i].K;
         }
      if(temp_mx.PointCount==1)
         {
         RangeSG->RowCount=2;
         RangeSG->Cells[0][1]="";
         RangeSG->Cells[1][1]="";
         RangeSG->Cells[2][1]="";
         RangeSG->Cells[3][1]="";
         RangeSG->Cells[4][1]="";
         RangeSG->Cells[5][1]="";
         }
      else
         RangeSG->RowCount=temp_mx.PointCount;

      for(int i=0;i<temp_mx.PointCount-1;i++)
         {
         RangeSG->Cells[0][i+1]=i+1;
         RangeSG->Cells[1][i+1]=temp_mx.Points[i].f;
         RangeSG->Cells[2][i+1]=temp_mx.Points[i].Q;
         RangeSG->Cells[3][i+1]=temp_mx.Points[i+1].f;
         RangeSG->Cells[4][i+1]=temp_mx.Points[i+1].Q;
         RangeSG->Cells[5][i+1]=temp_mx.Kd[i];
         }
      }
   if(OnlyPoints)
      return;
   KEdit->Text=temp_mx.K;
   ViscosityEdit->Text=temp_mx.Viscosity;
   DatePicker->DateTime=temp_mx.Date;
   MXTypeRadioGroup->ItemIndex=temp_mx.MXType;
   if(temp_mx.ArgumentMX==TTPRMX::FlowRate)
      ArgumentRadioGroup->ItemIndex=1;
   else
      ArgumentRadioGroup->ItemIndex=0;
   DescriptionMemo->Text=temp_mx.Description;
   ValidNowCheckBox->Checked=temp_mx.ValidNow;
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::EditPointButtonClick(TObject *Sender)
{
  double f,K,Q;
  //
   if(temp_mx.PointCount==0)
      return;
   int PointIndex=SG->Row-1;
   if(PointIndex<0)
      return;
   f=temp_mx.Points[PointIndex].f;
   K=temp_mx.Points[PointIndex].K;
   Q=temp_mx.Points[PointIndex].Q;

   if(TPRPointForm->EditPoint(f,K,Q)==true)
      {
      temp_mx.Points[PointIndex].f=f;
      temp_mx.Points[PointIndex].K=K;
      temp_mx.Points[PointIndex].Q=Q;
      SortPoints(&temp_mx);
      MXtoForm(true);
      }
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::DeletePointButtonClick(TObject *Sender)
{
   if(temp_mx.PointCount==0)
      return;
   int PointIndex=SG->Row-1;
   if(PointIndex<0)
      return;
   if(Application->MessageBox("������� �����?","�������������",MB_ICONSTOP|MB_YESNO)==ID_YES)
      {
      temp_mx.Points[PointIndex].f=0;
      temp_mx.Points[PointIndex].K=0;
      temp_mx.Points[PointIndex].Q=0;
      SortPoints(&temp_mx);
      temp_mx.PointCount--;
      MXtoForm(true);
      }
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::FormtoMX()
{
   temp_mx.K=KEdit->Text.ToDouble();
   temp_mx.Viscosity=ViscosityEdit->Text.ToDouble();
   temp_mx.Date=DatePicker->DateTime;
   temp_mx.MXType=MXTypeRadioGroup->ItemIndex;
   if(ArgumentRadioGroup->ItemIndex==1)
      temp_mx.ArgumentMX=TTPRMX::FlowRate;
   else
      temp_mx.ArgumentMX=TTPRMX::Frequency;
   memset(temp_mx.Description,0,TTPRMX_DescriptionTextSize);
   int length=DescriptionMemo->Text.Length();
   if(length>= TTPRMX_DescriptionTextSize)
      length=DescriptionMemo->Text-1;
   memcpy(temp_mx.Description,DescriptionMemo->Text.c_str(),length);
   temp_mx.ValidNow=ValidNowCheckBox->Checked;
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::EnableControls(bool ReadOnly)
{
   ViscosityEdit->ReadOnly=ReadOnly;
//   SelectProtokolButton->Enabled=!ReadOnly;
//   ClearProtokolButton->Enabled=!ReadOnly;
   DatePicker->Enabled=!ReadOnly;
   MXTypeRadioGroup->Enabled=!ReadOnly;
   AddPointButton->Enabled=!ReadOnly;
   EditPointButton->Enabled=!ReadOnly;
   DeletePointButton->Enabled=!ReadOnly;
   KEdit->ReadOnly=ReadOnly;
   ArgumentRadioGroup->Enabled=!ReadOnly;
   ValidNowCheckBox->Enabled=!ReadOnly;
   DescriptionMemo->ReadOnly=ReadOnly;
   CanEdit=!ReadOnly;
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::RangeSGDblClick(TObject *Sender)
{
   if(!CanEdit) return;
   if(temp_mx.PointCount < 2)
      return;
   int RangeIndex=RangeSG->Row-1;
   if(RangeIndex<0)
      return;
   if(TPRRangeForm->Edit(&temp_mx,RangeIndex))
      MXtoForm(true);
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::ArgumentRadioGroupClick(TObject *Sender)
{
   if(ArgumentRadioGroup->ItemIndex==1)
      temp_mx.ArgumentMX=TTPRMX::FlowRate;
   else
      temp_mx.ArgumentMX=TTPRMX::Frequency;
   SortPoints(&temp_mx);
   MXtoForm(true);
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::SGKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key==VK_RETURN)
      EditPointButtonClick(0);
}
//---------------------------------------------------------------------------
void __fastcall TTPRMXForm::RangeSGKeyDown(TObject *Sender, WORD &Key,
      TShiftState Shift)
{
   if(Key==VK_RETURN)
      RangeSGDblClick(0);
}
//---------------------------------------------------------------------------

