//---------------------------------------------------------------------------

#ifndef TPRMXForm_unitH
#define TPRMXForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Buttons.hpp>
#include <ComCtrls.hpp>
#include <ExtCtrls.hpp>
#include <Grids.hpp>
#include <ValEdit.hpp>
#include "NumberEdit.h"
#include "tpr.h"
//---------------------------------------------------------------------------
class TTPRMXForm : public TForm
{
__published:	// IDE-managed Components
   TLabel *Label2;
   TRadioGroup *MXTypeRadioGroup;
   TDateTimePicker *DatePicker;
   TLabel *Label1;
   TBitBtn *OkButton;
   TBitBtn *CancelButton;
   TNumberEdit *ViscosityEdit;
   TNumberEdit *KEdit;
   TLabel *Label4;
   TGroupBox *PointGroupBox;
   TStringGrid *SG;
   TBitBtn *AddPointButton;
   TGroupBox *RangeGroupBox;
   TStringGrid *RangeSG;
   TRadioGroup *ArgumentRadioGroup;
   TCheckBox *ValidNowCheckBox;
   TBitBtn *EditPointButton;
   TBitBtn *DeletePointButton;
   TGroupBox *DescGroupBox;
   TMemo *DescriptionMemo;
   void __fastcall AddPointButtonClick(TObject *Sender);
   void __fastcall EditPointButtonClick(TObject *Sender);
   void __fastcall DeletePointButtonClick(TObject *Sender);
   void __fastcall RangeSGDblClick(TObject *Sender);
   void __fastcall ArgumentRadioGroupClick(TObject *Sender);
   void __fastcall SGKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);
   void __fastcall RangeSGKeyDown(TObject *Sender, WORD &Key,
          TShiftState Shift);

private:	// User declarations
   TTPRMX temp_mx;
   void __fastcall MXtoForm(bool OnlyPoints=false);
   void __fastcall FormtoMX();
   void __fastcall EnableControls(bool ReadOnly);
   bool CanEdit;
public:		// User declarations
   __fastcall TTPRMXForm(TComponent* Owner);
   __fastcall ~TTPRMXForm();
   void __fastcall ViewMX(TTPRMX* mx);
   bool __fastcall EditMX(TTPRMX* mx);
};
//---------------------------------------------------------------------------
extern PACKAGE TTPRMXForm *TPRMXForm;
//---------------------------------------------------------------------------
#endif
