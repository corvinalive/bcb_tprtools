//---------------------------------------------------------------------------
#include <vcl.h>
#include "tpr.h"

#ifndef TPRDLLInterfaceH
#define TPRDLLInterfaceH
//---------------------------------------------------------------------------
extern IStorage* TprRoot;

class TTPRDLLInterface
{
protected:
    void* __fastcall ManageTPRMX(TActionManage Action, void* Data, int Reserved);
private:
public:
   static void SelectTPR(TComponent* Owner,IStorage* RootStorage, TTPR* TPR);
   static void ManageTPR(TComponent* Owner, IStorage* RootStorage);
   static void ViewTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR);
   static bool EditTPR(TComponent* Owner,/*in*/IStorage* RootStorage,TTPR* TPR) ;
   static void GetTPR(/*in*/IStorage* RootStorage,/*in*/int ID,/*out*/TTPR* TPR);
   static void SelectTPRwithMXList(/*in*/TComponent* Owner,/*in*/IStorage* RootStorage,
                           /*out*/TTPR* TPR, TList* MX, TOnManageEvent OnManage);
   static void GetTPRnMXList_(IStorage* RootStorage, int TPR_ID, TTPR* TPR,TList* MX,
                        TOnManageEvent OnManage);




   static void __fastcall OpenRootStorage(/*in*/IStorage* RootStorage);
// ������ �� ��������� TPR � ��������� ��������� � TList
   static void __fastcall LoadTPRs(/*in*/TList* TPRs);
   static void __fastcall SaveTPRs(/*in*/TList* TPRs);
   static void __fastcall SaveTPR(TTPR* tpr);
   static void __fastcall DeleteTPR(/*in*/int ID);
   static void __fastcall GetTPRFromID(/*in*/int ID,/*out*/TTPR* tpr);
   static void __fastcall LoadTPRMX(int TPR_ID,/*in*/TList* TPRMXList,TOnManageEvent OnManage);
   static void __fastcall SaveTPRMX(/*in*/int TPR_ID,/*in*/ TList* TPRMXList);
   static void __fastcall DeleteTPRMX(/*in*/int TPR_ID,/*in*/int MX_ID);
   static void __fastcall GetTPRnMXList(int ID,TTPR* TPR,TList* MXList,TOnManageEvent);

public:
   __fastcall TTPRDLLInterface();
   bool __fastcall Edit(TComponent* Owner, IStorage* RootStorage, TTPR* TPR);
   void __fastcall View(TComponent* Owner, IStorage* RootStorage, TTPR* TPR);
   void __fastcall Manage(TComponent* Owner, IStorage* RootStorage);
   void __fastcall Select(TComponent* Owner, IStorage* RootStorage, TTPR* TPR);
   void __fastcall Get(IStorage* RootStorage, int ID, TTPR* TPR);

   void __fastcall SelectwithMXList(TComponent* Owner, IStorage* RootStorage, TTPR* TPR, TList* MXList);
   void __fastcall GetTPRnMXList(IStorage* RootStorage, int TPR_ID, TTPR* TPR,TList* MX);
   int __fastcall AddMX(IStorage* RootStorage, int TPR_ID, TTPRMX* MX);

};
//---------------------------------------------------------------------------
#endif
