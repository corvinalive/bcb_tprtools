//---------------------------------------------------------------------------

#ifndef TPRSelectForm_unitH
#define TPRSelectForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include <Grids.hpp>

#include "tpr.h"
#include "TPRForm_unit.h"
#include <Buttons.hpp>
#include <ImgList.hpp>
//---------------------------------------------------------------------------
class TTPRSelectForm : public TForm
{
__published:	// IDE-managed Components
	TStringGrid *MMGrid;
   TLabel *Label1;
   TBitBtn *OkButton;
   TBitBtn *CancelButton;
   TBitBtn *ViewButton;
   TBitBtn *EditButton;
   TBitBtn *AddButton;
   TBitBtn *DeleteButton;
   TImageList *ImageList1;
   TEdit *Edit1;
   TLabel *Label2;
	void __fastcall ViewButtonClick(TObject *Sender);
	void __fastcall AddButtonClick(TObject *Sender);
	void __fastcall EditButtonClick(TObject *Sender);
	void __fastcall DeleteButtonClick(TObject *Sender);
   void __fastcall MMGridDblClick(TObject *Sender);
   void __fastcall MMGridDrawCell(TObject *Sender, int ACol, int ARow,
          TRect &Rect, TGridDrawState State);
   void __fastcall MMGridMouseDown(TObject *Sender, TMouseButton Button,
          TShiftState Shift, int X, int Y);
   void __fastcall Edit1Change(TObject *Sender);
private:	// User declarations
   bool IsManage;
   void __fastcall Sort();
//   int __fastcall CompareNames(void *Item1, void *Item2);
public:		// User declarations
	__fastcall TTPRSelectForm(TComponent* Owner);
	void __fastcall SelectTPR(TTPR* TPR);
	TList* TPRs;
	TList* TPRs1;
	TTPR* __fastcall GetTPRIndex();
	virtual void __fastcall CreateParams(Controls::TCreateParams &Params);
	void __fastcall FillGrid();
public:		// User declarations
	__property TTPR* SelectedTPR  = { read=GetTPRIndex };

	__fastcall ~TTPRSelectForm();
	TTPRForm* MMForm;
	void __fastcall Manage();
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
