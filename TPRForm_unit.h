//---------------------------------------------------------------------------

#ifndef TPRForm_unitH
#define TPRForm_unitH
//---------------------------------------------------------------------------
#include <Classes.hpp>
#include <Controls.hpp>
#include <StdCtrls.hpp>
#include <Forms.hpp>
#include "NumberEdit.h"
#include <ComCtrls.hpp>

#include "tpr.h"
#include <Buttons.hpp>
//---------------------------------------------------------------------------
class TTPRForm : public TForm
{
__published:	// IDE-managed Components
   TBitBtn *OkButton;
   TBitBtn *CancelButton;
   TGroupBox *GroupBox2;
   TListBox *MXListBox;
   TBitBtn *EditMXButton;
   TBitBtn *DeleteMXButton;
   TBitBtn *AddMXButton;
   TGroupBox *GroupBox1;
   TLabel *Label13;
   TLabel *Label15;
   TLabel *Label1;
   TLabel *Label5;
   TLabel *Label2;
   TLabel *Label4;
   TLabel *Label6;
   TLabel *Label7;
   TEdit *LocationEdit;
   TEdit *OwnerEdit;
   TEdit *CompanyEdit;
   TEdit *SNEdit;
   TEdit *ModelEdit;
   TNumberEdit *DEdit;
   TNumberEdit *QmaxEdit;
   TEdit *UUNEdit;
   TBitBtn *ViewButton;
   void __fastcall EditMXButtonClick(TObject *Sender);
   void __fastcall AddMXButtonClick(TObject *Sender);
   void __fastcall DeleteMXButtonClick(TObject *Sender);
   void __fastcall ViewButtonClick(TObject *Sender);
   void __fastcall MXListBoxDrawItem(TWinControl *Control, int Index,
          TRect &Rect, TOwnerDrawState State);
private:	// User declarations
	void __fastcall EnableControls();
   TList* MX;
	TList* DeletedMX;
   void __fastcall FillMXListBox();
	void __fastcall ClearMXData();
	void __fastcall SaveMXData();
   void __fastcall FillMXData();
public:		// User declarations
	__fastcall TTPRForm(TComponent* Owner);
	bool CanEdit;
	void __fastcall CreateParams(Controls::TCreateParams &Params);


	bool IsNew;
	int ID;
	void __fastcall TPRToForm(TTPR* TPR);
	void __fastcall FormToTPR(TTPR* TPR);
public:		// User declarations
	void __fastcall ViewTPR(TTPR* TPR);
	bool __fastcall EditTPR(TTPR* TPR);
	bool __fastcall NewTPR(TTPR* TPR);
   __fastcall ~TTPRForm();
};
//---------------------------------------------------------------------------
//---------------------------------------------------------------------------
#endif
